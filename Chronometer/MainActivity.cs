﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util;

namespace Chronometer
{


	[Activity (Label = "@string/Chronometer", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		TimeSpan accumulatedTime = new TimeSpan();
		TextView timeView;
		Button startStopButton, resetButton;
		ISharedPreferences  data;
		private Handler handler;
		private bool isCounting = false;
		DateTime lastTime;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			timeView = (TextView) FindViewById (Resource.Id.timeView);
			startStopButton = (Button) FindViewById (Resource.Id.startStopButton);
			resetButton = (Button)FindViewById (Resource.Id.resetButton);

			startStopButton.Click += (sender, EventArgs) => startStop ();
			resetButton.Click += (sender, EventArgs) => reset();

			data =  GetSharedPreferences ("counter", FileCreationMode.Private);
			handler = new Handler ();
		}

		private void showCount(){
			timeView.Text = accumulatedTime.Hours.ToString ("00") + ":" + accumulatedTime.Minutes.ToString ("00") + ":" + accumulatedTime.Seconds.ToString ("00") + "." + accumulatedTime.Milliseconds.ToString ("000");
			}

		private void increment(){
			accumulatedTime += DateTime.UtcNow.Subtract (lastTime);
			lastTime = DateTime.UtcNow;
			showCount ();
		}

		private void reset(){
			accumulatedTime = new TimeSpan ();
			showCount ();
			isCounting = false;
			startStopButton.Text = Resources.GetString (Resource.String.Start);
		}

		protected override void OnSaveInstanceState (Bundle bundle){
			base.OnSaveInstanceState (bundle);
			bundle.PutDouble ("timeSpan", accumulatedTime.TotalMilliseconds);
			bundle.PutBoolean ("isCounting", isCounting);
			bundle.PutLong ("lastTime", lastTime.Ticks);
		}

		protected override void OnRestoreInstanceState (Bundle bundle){
			base.OnRestoreInstanceState (bundle);
			accumulatedTime=TimeSpan.FromMilliseconds(bundle.GetDouble ("timeSpan"));
			isCounting = bundle.GetBoolean ("isCounting");
			lastTime = new DateTime(bundle.GetLong ("lastTime"));
			showCount ();
			Restore ();
		}

		protected override void OnResume(){
			base.OnResume ();
			accumulatedTime=TimeSpan.FromMilliseconds(data.GetFloat("timeSpan",0));
			isCounting = data.GetBoolean ("isCounting", false);
			lastTime = new DateTime(data.GetLong ("lastTime",0));
			showCount ();
			Restore ();
		}

		protected override void OnPause(){
			base.OnPause ();
			ISharedPreferencesEditor e = data.Edit ();
			e.PutFloat("timeSpan",(float)accumulatedTime.TotalMilliseconds);
			e.PutBoolean ("isCounting", isCounting);
			e.PutLong ("lastTime", lastTime.Ticks);
			e.Commit ();
		}

		private void Restore(){
			if (isCounting) {
				GenerateDelayedTick ();
				startStopButton.Text = Resources.GetString (Resource.String.Stop);
			}
			else{
				startStopButton.Text = Resources.GetString (Resource.String.Start);
			}
		}

		private void startStop(){
			isCounting = !isCounting;
			if (isCounting) {
				lastTime = DateTime.UtcNow;
				GenerateDelayedTick ();
				startStopButton.Text = Resources.GetString (Resource.String.Stop);
			} else {
				isCounting = false;
				startStopButton.Text = Resources.GetString (Resource.String.Start);
			}
		}

		private void GenerateDelayedTick(){
			handler.PostDelayed (OnTick,50);
		}

		private void OnTick(){
			if (isCounting) {
				increment ();
				GenerateDelayedTick ();
			}
		}


	}

}

